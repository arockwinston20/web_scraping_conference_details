import requests
from bs4 import BeautifulSoup as soup
from urllib.request import urlopen as UReq
import re


highlight = "Nil";
organizer_link = "Nil";
organizer_name = "Nil";
keyword = "Nil"


def qconsf_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        conference_list = html_dom.find_all('div', {'class': 'col-md-8 panel_left'})
        organizer_list = html_dom.find_all('div', {'class': 'row qcon-tagline sf'})
        organizer_name = str(organizer_list[0].find_all('h4')[1].text.strip())
        organizer_link = str(organizer_list[0].find("ul").find_all('li')[3].a['href'].strip())
        highlight = str(conference_list[0].p.text.strip())
        return highlight, organizer_link, organizer_name, keyword
    except Exception:
        return highlight, organizer_link, organizer_name, keyword

def agiledevopseast_techwell_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        parsed_output = soup(response.content, 'html.parser')
        highlight = str(parsed_output.find_all('div', {'class': 'pane-content'})[4].p.text.strip())
        keywords = parsed_output.find_all('div', {'class': 'pane-content'})[4].ul.find_all('li')
        organizer_link = str(parsed_output.find_all('div', {'class': 'footer-logo'})[0].a["href"]).split('/')[-1]
        organizer_name = str(parsed_output.find('section', {'id': 'block-block-5'}).div.a.img['alt']).split(' ')[0]
        keyword_list = []
        for keyword in keywords:
            keyword_list.append(str(keyword.text).strip())
        keyword = ", ".join(keyword_list)

        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def usenix_org_2(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        highlight = str(html_dom.find_all('div', {'class': 'field field-name-field-body field-type-text-long field-label-hidden'})[0].div.div.h2.text).strip()
        organizer_link = str(html_dom.head.link['href'].rsplit('/', 4)[0])
        organizer_name = str(html_dom.find_all('section', {"class":"block block-block 113"})[0].div.p.text).strip().split(' ', 7)[-1].split('.')[0]
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def gotober_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        highlight = re.sub('\s\s+', ' ', str(html_dom.find_all('div', {'class': "col-sm-6"})[1].p.text))
        organizer_link = str(html_dom.find('footer', {'class': "footer"}).find('div', {'class': "col-sm-6"}).div.a['href'])
        organizer_name = str(html_dom.
                             find('footer', {'class': "footer"}).
                             find('div', {'class': "col-sm-6"}).
                             div.a.img['alt'])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def allthingsopen_org(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        organizer_name = str(html_dom.find('h5', {'class': "hero__lead-in"}).text)
        highlight = str(html_dom.find('h5', {'class': "expected__tagline"}).text)
        organizer_link = str(html_dom
                             .find('nav', {'class': "social-icons"})
                             .find_all('a')[2]['href'])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def puppet_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        parsed_output = soup(response.text, 'html.parser')
        highlight = str(parsed_output.find('div', {"class" :"body"}).find_all('p')[0].text).strip() + " " + str(parsed_output.find('div', {"class" :"body"}).find_all('p')[1].text).strip()
        organizer_name = str(parsed_output.find('div', {"class" :"footer-menu-container"}).find('div').img['class'][0]).split('-')[0].strip()
        organizer_link = str(url.rsplit("/", 1)[0])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def dt_x_io(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        parsed_output = soup(response.text, 'html.parser')
        highlight = ' '.join([str(high.text).strip() for high in parsed_output.find_all('div', {"class" :"content-body ct-desc"})[0].find_all('p')])
        organizer_name = str(parsed_output.find_all('font', {"color" :"#9c9c94"})[0].span.text).split(' | ')[0].split(' – ')[-1]
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def jaxlondon_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        highlight = str(html_dom.find_all('div', {"class" :"col-md-8 col-sm-7"})[0].p.text).strip()
        organizer_name = str(html_dom.find_all('div', {"class" :"widget widget_text gdlr-item gdlr-widget"})[0].h3.text).strip()
        organizer_link = str(html_dom.find('div', {"class" :"gdlr-logo"}).a['href'])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def usenix_org_1(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        highlight = str(html_dom.find_all('div', {'class': 'field field-name-field-body field-type-text-long field-label-hidden'})[0].div.div.p.text).strip()
        organizer_link = url.rsplit('/', 2)[0]
        organizer_name = str(html_dom.find('section', {"class":"block block-block 121"}).div.div.p.text).strip().split(' ', 7)[-1].split('.')[0]
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def skillsmatter_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        parsed_output = soup(response.text, 'html.parser')
        organizer_name = str(parsed_output.find('li', {'class':'super-nav__options__main super-nav__options__main--logo'}).a.img['alt'].rsplit(' ', 1)[0])
        highlight = str(parsed_output.find('div', {'class':'module__inner'}).p.text).strip()
        organizer_link = str(url.rsplit("/", 2)[0])+str(parsed_output.find('div', {'class':'inline module--sponsors__listing--level1'}).a['href'])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def ansible_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        parsed_output = soup(response.text, 'html.parser')
        highlight = str(parsed_output.find('span', {'id':"hs_cos_wrapper_module_1558552109506660_"}).p.text).strip()
        organizer_name = str(parsed_output.find('div', {'class':"col-md-6 col-lg-4 pt-5 px-4 bg-gray-dark order-1 order-md-2"}).h5.text).strip()
        organizer_link = str(parsed_output.find('div', {'class':"col-md-6 col-lg-4 pt-5 px-4 bg-gray-dark order-1 order-md-2"}).a['href'])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def cloudfoundry_org(url):
    global highlight, organizer_name, organizer_link, keyword
    highlight = "Nil"; organizer_link = "Nil"; organizer_name = "Nil"; keyword = "Nil"
    try:
        response = requests.get(url)
        parsed_output = soup(response.text, 'html.parser')
        organizer_link= str(parsed_output.find('li', {'class': "site-branding"}).a['href'])
        highlight = str(parsed_output.find('div', {'class': "event-description"}).find_all('p')[0].text).strip() + str(parsed_output.find('div', {'class': "event-description"}).find_all('p')[1].text).strip()
        organizer_name = str(parsed_output.find('div', {'class': "float75 btm-copyright full-800"}).p.text).strip().split(",")[0].split(' ', 1)[1]
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def devopstalks_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        organizer_link = url[:-1] + str(html_dom.find('div', {'class': 'col-md-offset-3 col-md-6 col-sm-12 top'}).a['href'])
        highlight = str(html_dom.find('div', {'class': 'caption'}).p.text).strip()
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def hashicorp_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        highlight = str(html_dom.find('span', {'class':'sub-text'}).text).strip()
        organizer_name = str(html_dom.find('h4', {'class':'description'}).text).strip()
        organizer_link = str(html_dom.find_all('div', {'class':'buttons'})[0].a['href']).strip()
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def devopsdays_org(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        for_remove_tag = html_dom.find_all('div', {'class':'col-md-6 col-lg-3 footer-nav-col'})[2]
        removed_tag = for_remove_tag.h3
        removed_tag.decompose()
        highlight = str(for_remove_tag.text).split('.')[0]
        organizer_link = str(url.rsplit('/', 4)[0] + for_remove_tag.a['href'])
        organizer_name = str(for_remove_tag.a.text).split(' ')[-1]
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def dashcon_io(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        highlight = str(html_dom.find('div', {'class':'col-sm-10 offset-sm-1 pt-2'}).p.text)
        organizer_link = str(html_dom.find('div', {'class':'col-2 bg-purple py-1'}).a['href'])
        organizer_name = str(html_dom.find('div', {'class': 'col-2 bg-purple py-1'}).a['id'].split('-')[0]).upper()
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def hashiconfeu_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        highlight = str(html_dom.find('div', {'class': 'text'}).p.text).strip()
        organizer_name = str(html_dom.find('div', {'class': 'btm'}).p.text).strip().rsplit(' ', 2)[-1]
        organizer_link = str(html_dom.find_all('div', {'class': 'buttons'})[0].a['href']).strip()
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def events_itrevolution_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        highlights = html_dom.find_all('div', {'class': 'wrap'})[5].find_all('p')
        organizer_link = html_dom.find('li', {'class': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-22'}).a['href']
        organizer_name = str(html_dom.find('div', {'class': 'footer-cred left'}).text.split('. ')[0].split("9")[1]).strip()
        highlights[0].decompose()
        highlights.pop(0)
        highlights_list = []
        for high in highlights:
            highlights_list.append(high.text)
        highlight = ' '.join(highlights_list)

        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def lfasiallc_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        highlights = html_dom.find('div',{'class': 'vc_custom_1548172269574'}).div.find_all('h4')
        organizer_name = ' '.join(str(html_dom.find('div',{'class': 'col span_5'}).p.text).strip().split(' | ')[0].split(' ')[3:])
        highlight = ' '.join([val.text for val in highlights[3:]])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def containerdays_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        highlight = ' '.join([re.sub('\s\s+', ' ', str(val.text).strip()).replace('#',"") for val in html_dom.find('div', {'class':'new_about'}).article.find_all('p')])
        organizer_name = re.sub('\s\s+', ' ', str(html_dom.find('div', {'class':'mailer-wrapper'}).p.text).strip().replace('\n', ' ')).split(' ')[-1][:-1]
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def qconnewyork_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        organizer_name = str(html_dom.find('h2', {'class':"center pane-title section-title"}).text).strip()
        highlight = str(html_dom.find('div', {'class':"row qcon-tagline ny"}).h4.text).strip()
        organizer_link = url
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def rootconf_in(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        highlight = " ".join([highlight.text for highlight in html_dom.find_all('div', {"class": "column"})[0].ul.find_all('li')])
        organizer_name = str(html_dom.find('a', {'class': "nav-item nav-home"}).text).strip()
        organizer_link = str(html_dom.find('a', {'class': "nav-item nav-home"})['href'])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def devopsonline_co_uk(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        highlight = ' '.join([high.text for high in html_dom.find('div', {'class':'wpb_text_column wpb_content_element'}).find_all('div', "wpb_wrapper")])
        organizer_link = str(html_dom.find('li', {'id': "menu-item-14279"}).a['href'])
        organizer_name = str(html_dom.find_all('span', {'class':"w-nav-title"})[1].text)
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword
def gotoams_nl(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        highlight = " ".join([str(high.text).strip() for high in html_dom.find('div', {'class':'section__content emph-text'}).find_all('p')])
        organizer_link = str(html_dom.find_all('div', {'class':'col-sm-6'})[-1].div.a['href'])
        organizer_name = str(html_dom.find_all('div', {'class':'col-sm-6'})[-1].div.a.img['alt'])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def usenix_org_3(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        highlight = " ".join([str(high.text).strip() for high in html_dom.find_all('div', {'class': 'field field-name-field-body field-type-text-long field-label-hidden'})[0].div.div.find_all('p')])
        organizer_link = url.rsplit('/', 2)[0]
        organizer_name = str(html_dom.find('section', {"class": "block block-block 121"}).div.div.p.text).strip().split(' ', 7)[-1].split('.')[0]
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def mdoyvr_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        highlight = str(html_dom.find_all("div", {'class': 'entry-content'})[0].p.text)
        organizer_name = str(html_dom.find("div", {'class': 'milestone-header'}).strong.text.split(' ')[0])
        organizer_link = str(html_dom.find("li", {'id': 'menu-item-894'}).a['href'])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def tmt_knect365_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        highlight = ' '.join([str(high.text).strip() for high in html_dom.find('div', {'class': 'video-text'}).div.find_all('p')])
        organizer_link = str(html_dom.find('div', {'class': 'col-sm-7 col-sm-pull-5 col-lg-4 col-lg-pull-8 informa-tech-logo-footer'}).a['href'])
        organizer_name = str(html_dom.find('div', {'class': 'col-sm-7 col-sm-pull-5 col-lg-4 col-lg-pull-8 informa-tech-logo-footer'}).a.img['alt']).rsplit(' ', 1)[0]
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def devopsconference_de(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        highlight = str(html_dom.find_all('div', {'class': "gdlr-item-title-head"})[0].div.text)
        organizer_link = str(html_dom.find_all('div', {'class': "gdlr-header-container container"})[0].div.a['href'])
        organizer_name = str(html_dom.find('div', {'id': "speaker-desktop-background"}).div.div.h2.strong.text).split(' ')[0]
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def conferences_oreilly_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        highlight = str(html_dom.find('div', {'id': "reasons_row"}).div.p.text)
        organizer_name = str(html_dom.find('div', {'id': "copyright"}).div.p.text.split(',')[1]).strip().split(' ')[0]
        organizer_link = str(html_dom.find_all('div', {'class': "footercol col-1-5"})[2].ul.find_all('li')[3].a['href'])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def puppet_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        highlight = str(html_dom.find_all('div', {'class':'col-cell'})[0].div.p.text).strip()
        organizer_link = url.rsplit("/", 2)[0] + str(html_dom.find_all('div', {'data-category': 'Connect'})[0].ul.find_all('li')[0].a['href'])
        organizer_name = str(html_dom.find_all('div', {'class':'newsletter-row'})[0].text.split(' ')[0])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def devops_barcelona(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        organizer_name = str(html_dom.find_all('p', {'class':"text-alt"})[-1].text).split(' - ')[1].split(' ')[0]
        highlight = " ".join([str(high.text).strip() for high in html_dom.find('div', {'class':"col-sm-7 align-left"}).find_all('p')])
        organizer_link = str(url + html_dom.find('ul', {'class':"footer-nav"}).find_all('li')[1].a['href'])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def devopsdays_org_2(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        highlight = str(html_dom.find_all('p', {'class':'footer-content'})[0].text).strip()
        organizer_name = str(html_dom.find('a', {'class':'navbar-brand'}).text)
        organizer_link = str(url.rsplit('/', 4)[0])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def monitorama_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        organizer_name = ' '.join(str(html_dom.find('div', {'class':'text-center container-fluid copyright'}).p.text).split(' ')[3:])
        highlight = str(html_dom.find('div', {'class':'text-center tagline'}).text).strip()
        organizer_name = url
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def agiledevopswest_techwell_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        organizer_name = str(html_dom.find('section', {'id':'block-block-5'}).div.a.img['alt']).split(' ')[0]
        highlight = str(html_dom.find_all('div', {'class':'pane-content'})[3].p.text).strip()
        keyword = ', '.join([str(high.text).strip() for high in html_dom.find_all('div', {'class':'pane-content'})[3].ul.find_all('li')])
        organizer_link = str(url + html_dom.find_all('li', {'class':'first leaf'})[5].a['href'])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def texaslinuxfest_org(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        highlight = str(html_dom.find('article', {'id': "what-is-txlf"}).section.p.text)
        organizer_name = ' '.join(html_dom.find('footer').find_all('div')[1].text.split(' ')[1:4])
        organizer_link = str(url + html_dom.find('ul',{'class':""}).li.a['href'][1:])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def rigadevdays_lv(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        organizer_name = ' '.join(html_dom.find('p', {'class':'text-muted'}).text.split(',')[0].split(' ')[2:])
        highlight = str(html_dom.find('div', {'class':'col-md-10 text-center'}).h2.text +' '+ html_dom.find('div', {'class':'col-md-10 text-center'}).p.text)
        organizer_link = str(html_dom.find('nav', {'id': "menu-topmenu"}).find_all('a')[-1]['href'])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def githubsatellite_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        organizer_name = str(html_dom.find('div', {'class': "logo"}).h1.text.split(' ')[-1])
        highlight = str(html_dom.find('h2', {'class': "p-lg mx-auto col-md-9"}).text)
        organizer_link = str(html_dom.find('div', {'class': "d-flex flex-items-center flex-justify-center mt-5 mt-md-"}).a['href'])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def gluecon_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        organizer_name = ' '.join(str(html_dom.find('div', {'class':'col span_5'}).p.text).split(' - ')[0].split(' ')[2:])
        highlight = str(' '.join([high.text for high in html_dom.find_all('div', {'class':'wpb_wrapper'})[3].find('div', {'class':'wpb_wrapper'}).find_all('div')[:-1]])).strip()
        organizer_link = str(html_dom.find('li', {'id': "menu-item-46"}).a['href'])
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def chefconf_chef_io(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        organizer_link = str(html_dom.find_all('div', {'class':'editor-content'})[-2].p.a['href'])
        highlight = str(html_dom.find_all('div', {'class':'editor-content'})[3].p.text)
        organizer_name = organizer_link.split(".")[-2].upper()
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def interop_com(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        organizer_link = str(html_dom.find('div', {'class':'col-xs-2 col-md-1'}).a['href'])
        organizer_name = str(html_dom.find('li', {'class':'iribbon-mainlink'}).a.text)
        highlight = str(html_dom.find_all('div', {'class':'field field-name-field-paragraph-text field-type-text-long field-label-hidden'})[1].div.div.h3.text)
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def events_linuxfoundation_org(url):
    global highlight, organizer_name, organizer_link, keyword
    try:
        response = requests.get(url)
        html_dom = soup(response.text, 'html.parser')
        highlight = str(html_dom.find_all('p', {'style':'text-align: center;'})[0].text)
        organizer_name =' '.join(html_dom.find('div', {'class':'col span_5'}).find_all('p')[1].text.split('. ')[0].split(' ')[-3:])
        organizer_link = url.rsplit('/', 3)[0]
        return highlight, organizer_link, organizer_name, keyword
    except Exception as e:
        return highlight, organizer_link, organizer_name, keyword

def unmatched_url():
    global highlight, organizer_name, organizer_link, keyword
    return highlight, organizer_link, organizer_name, keyword


def match_links(url):
    global highlight, organizer_name, organizer_link, keyword
    highlight = "Nil"; organizer_name = "Nil"; organizer_link = "Nil"; keyword = "Nil"
    if url == "https://qconsf.com/":
        return qconsf_com(url)
    elif url == "https://agiledevopseast.techwell.com/":
        return agiledevopseast_techwell_com(url)
    elif url == "https://www.usenix.org/conference/lisa19":
        return usenix_org_2(url)
    elif url == "https://gotober.com/2019":
        return gotober_com(url)
    elif url == "https://allthingsopen.org/":
        return allthingsopen_org(url)
    elif url == "https://puppet.com/puppetize":
        return puppet_com(url)
    elif url == "https://www.ipexpoeurope.com/":
        return dt_x_io(url)
    elif url == "https://jaxlondon.com/":
        return jaxlondon_com(url)
    elif url == "https://www.usenix.org/conference/srecon19europe":
        return usenix_org_1(url)
    elif url == "https://skillsmatter.com/conferences/11723-cloudnative-london-2019":
        return skillsmatter_com(url)
    elif url == "https://www.ansible.com/ansiblefest":
        return ansible_com(url)
    elif url == "https://www.cloudfoundry.org/event/eusummit2019/":
        return cloudfoundry_org(url)
    elif url == "https://devopstalks.com":
        return devopstalks_com(url)
    elif url == "https://www.hashiconf.com":
        return hashicorp_com(url)
    elif url == "https://www.devopsdays.org/events/2019-minneapolis/welcome/":
        return devopsdays_org(url)
    elif url == "https://www.dashcon.io/":
        return dashcon_io(url)
    elif url == "https://hashiconfeu.hashicorp.com/":
        return hashiconfeu_com(url)
    elif url == "https://www.devopsdays.org/events/2019-amsterdam/welcome/":
        return devopsdays_org(url)
    elif url == "https://events.itrevolution.com/eur/":
        return events_itrevolution_com(url)
    elif url == "https://www.lfasiallc.com/events/kubecon-cloudnativecon-china-2019/":
        return lfasiallc_com(url)
    elif url == "https://www.containerdays.io/":
        return containerdays_com(url)
    elif url == "https://qconnewyork.com/":
        return qconnewyork_com(url)
    elif url == "https://rootconf.in/2019/":
        return rootconf_in(url)
    elif url == "http://www.devopsonline.co.uk/national-devops-conference/":
        return devopsonline_co_uk(url)
    elif url == "https://gotoams.nl/":
        return gotoams_nl(url)
    elif url == "https://www.usenix.org/conference/srecon19asia":
        return usenix_org_3(url)
    elif url == "https://mdoyvr.com/":
        return mdoyvr_com(url)
    elif url == "https://tmt.knect365.com/cloud-devops-world/":
        return tmt_knect365_com(url)
    elif url == "https://devopsconference.de/":
        return devopsconference_de(url)
    elif url == "https://conferences.oreilly.com/velocity/vl-ca":
        return conferences_oreilly_com(url)
    elif url == "https://puppet.com/puppetize/contributor-summit":
        return puppet_com(url)
    elif url == "https://devops.barcelona":
        return devops_barcelona(url)
    elif url == "https://www.devopsdays.org/events/2019-portugal/welcome/":
        return devopsdays_org_2(url)
    elif url == "http://monitorama.com/":
        return monitorama_com(url)
    elif url == "https://agiledevopswest.techwell.com/":
        return agiledevopswest_techwell_com(url)
    elif url == "https://2019.texaslinuxfest.org/":
        return texaslinuxfest_org(url)
    elif url == "https://rigadevdays.lv/":
        return rigadevdays_lv(url)
    elif url == "https://www.devopsdays.org/events/2019-toronto/welcome/" or url == 'https://devopsdays.org/events/2019-kiev/welcome/' or url == 'https://www.devopsdays.org/events/2019-sydney/welcome/':
        return devopsdays_org_2(url)
    elif url == "https://githubsatellite.com/":
        return githubsatellite_com(url)
    elif url == "http://gluecon.com":
        return gluecon_com(url)
    elif url == "https://chefconf.chef.io/":
        return chefconf_chef_io(url)
    elif url == "https://www.interop.com/":
        return interop_com(url)
    elif url == "https://events.linuxfoundation.org/events/kubecon-cloudnativecon-europe-2019/" or url == 'https://events.linuxfoundation.org/events/kubecon-cloudnativecon-north-america-2019/':
        return events_linuxfoundation_org(url)
    else:
        return unmatched_url()
