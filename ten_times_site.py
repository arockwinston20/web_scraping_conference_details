from bs4 import BeautifulSoup as soup
from urllib.request import urlopen as UReq
import re


def tenTime_site(url):
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        conference_list = html_dom.find('div', {'id': 'content'})
        high_light_list = conference_list.find('section', {'class': 'box'}).ul
        organizer_details = conference_list.find_all('table', {'class': 'table noBorder mng'})
        organizer_link = None; organizer_name = None
        for org in organizer_details:
            heading_three = org.find_all('tr')[-1].h3
            try:
                organizer_link = str(heading_three.a['href'])
                organizer_name = str(heading_three.a.text)
            except Exception:
                organizer_link = "Nil"
                organizer_name_stripped = str(heading_three.text).strip()
                organizer_name = organizer_name_stripped.rsplit(' ', 1)[0]
        if high_light_list == None:
            high_light_list = conference_list.find('section', {'class': 'box'}).p.text
            highlight_with_double_spaces = str(high_light_list).strip()
            highlight = re.sub('\s\s+', ' ', highlight_with_double_spaces)
        elif high_light_list.find('li'):
            high_light_li = high_light_list.find_all('li')
            highlight = ""
            for para in high_light_li:
                para_value = str(para.text).strip()
                if para_value[-1] == "." or para_value[-1] == "?":
                    highlight = highlight + para_value + " "
                else:
                    highlight = highlight + para_value + ". "
        else:
            highlight = "Nil"

        return highlight, organizer_link, organizer_name
    except Exception as e:
        highlight = "Nil"; organizer_link = "Nil"; organizer_name = "Nil"
        return highlight, organizer_link, organizer_name
