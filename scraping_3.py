from bs4 import BeautifulSoup as soup
from urllib.request import urlopen as UReq
import datetime
import xlsxwriter

jsonObj = {}
urls = ["", "https://10times.com/top100/blockchain", "https://www.gotodevops.org/"]

def website_3(urls):
    global row, jsonObj
    scrapurl = UReq(urls[2])
    scraphtml = scrapurl.read()

    website_name = "gotodevops.org"
    site_3 = {website_name: []}
    jsonObj.update(site_3)

    html_dom = soup(scraphtml, 'html.parser')
    conference_list = html_dom.find('div', {'id': 'conferences'})
    list = conference_list.find_all("a")

    conference_details = []

    for a_list in list:
        conference_list = str(a_list['href']).strip()
        conference_details_country_title = a_list.find_all("div", {"class": "col-12"})
        conference_date = str(a_list.find("div", {"class": "col-5 col-md-6 mr-0 pr-0"}).text).strip()
        conference_title = str(conference_details_country_title[0].text).strip().split(' »')[0]
        conference_location = str(conference_details_country_title[1].text).strip()
        print(conference_title)
        print(conference_location)
        print(conference_date)
        print(conference_list)

        conference_details.append({"Conference_Name": conference_title,
                                   "Location": conference_location,
                                   "Description": "Nil",
                                   "Date": conference_date,
                                   "Link": conference_list})
        print(conference_details)
    jsonObj["gotodevops.org"].append(conference_details)

website_3(urls)
print(jsonObj)