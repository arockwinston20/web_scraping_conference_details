from bs4 import BeautifulSoup as soup
from urllib.request import urlopen as UReq
import datetime
import xlsxwriter
import re

jsonObj = {}
urls = ["", "https://10times.com/top100/blockchain", "https://www.gotodevops.org/", "https://www.alltechconferences.com/etags/devops/"]

def website_4(urls):
    global row, jsonObj
    scrapurl = UReq(urls[3])
    scraphtml = scrapurl.read()

    website_name = "konfhub.com"
    site_4 = {website_name: []}
    jsonObj.update(site_4)

    html_dom = soup(scraphtml, 'html.parser')
    conference_list = html_dom.find_all('section', {'id': 'loop_event_taxonomy'})

    conference_details = []

    for post_list in conference_list:
        post = post_list.find_all("article")
        for from_article_tag in post:
            conference_name = (str(from_article_tag.find("div", {"class": "event-title"}).h2.a.text.strip()))
            date_with_spaces = str(from_article_tag.find("div", {"class": "event-title"}).span.text.strip())
            date = re.sub('\s\s+', ' ', date_with_spaces)
            conference_link = str(from_article_tag.find("div", {"class": "event-title"}).h2.a["href"].strip())
            conference_location = str(from_article_tag.find("div", {"class": "entry-details"}).p.text.strip()).split(": ")[1]
            description = str(from_article_tag.find("div", {"class": "entry-summary"}).p.text.strip()).split(" Read more")[0]

            print(conference_name)
            print(description)
            print(conference_location)
            print(date)
            print(conference_link)

            conference_details.append({"Conference_Name": conference_name,
                                       "Location": conference_location,
                                       "Description": description,
                                       "Date": date,
                                       "Link": conference_link})
            conference_details_list = [conference_name, conference_location,
                                       description, date, conference_link]


    jsonObj[website_name].append(conference_details)
    print("Details of the website: " + website_name + " is updated..")

website_4(urls)
print(jsonObj)