from bs4 import BeautifulSoup as soup
from urllib.request import urlopen as UReq

def all_tech_conf(url):
    try:
        scrapurl = UReq(url)
        scraphtml = scrapurl.read()
        html_dom = soup(scraphtml, 'html.parser')
        conference_list = html_dom.find('p', {'class': 'website'})
        a_tag_list = html_dom.find('div', {'class': 'post-meta'})
        organizer_link = str(conference_list.find("a", {"class": "frontend_website"}).text).strip()
        keywords_list = a_tag_list.find_all('a')
        keyword_concat = []
        for i, keyword in enumerate(keywords_list):
            if i == 0:
                continue
            keyword_concat.append(str(keyword.text).strip())
        keyword_concat = ', '.join(keyword_concat)
        organizer_name = 'Nil'
        return organizer_link, organizer_name, keyword_concat
    except Exception:
        organizer_link = "Nil"; organizer_name = "Nil"; keyword_concat = "Nil"
        return organizer_link, organizer_name, keyword_concat