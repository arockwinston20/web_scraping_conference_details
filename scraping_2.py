from bs4 import BeautifulSoup as soup
from urllib.request import urlopen as UReq
import datetime
import xlsxwriter

jsonObj = {}
urls = ["", "https://10times.com/top100/blockchain"]

def website_2(urls):
    global row, jsonObj
    scrapurl = UReq(urls[1])
    scraphtml = scrapurl.read()

    website_name = "10times.com"
    site_2 = {website_name: []}
    jsonObj.update(site_2)

    html_dom = soup(scraphtml, 'html.parser')
    table = html_dom.find_all('table', {'class': 'table table-responsive table-hover'})

    conference_details = []

    for for_rows in table:
        table_row = for_rows.find_all("tr")
        for for_table_data in table_row:
            table_data = for_table_data.find_all("td")
            if table_data == []:
                continue
            elif table_data[0].has_attr('colspan'):
                continue
            else:
                print(table_data[1].strong.a.text)
                conference_title = table_data[1].strong.a.text
                print(table_data[1].strong.a['href'])
                conference_link = table_data[1].strong.a['href']
                print(table_data[2].strong.text)
                conference_date = table_data[2].strong.text
                country = str(table_data[3].a.text).strip()
                state = str(table_data[3].small.text).strip()
                address = state+", "+country
                print(address)
                description = str(table_data[4].text).strip()
                print(description)

                conference_details.append({"Conference_Name": conference_title,
                                           "Location": address,
                                           "Description": description,
                                           "Date": conference_date,
                                           "Link": conference_link})
    jsonObj[website_name].append(conference_details)

website_2(urls)

print(jsonObj)