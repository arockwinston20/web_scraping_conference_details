from bs4 import BeautifulSoup as soup
from urllib.request import urlopen as UReq
import datetime
import xlsxwriter
import re



def Date():
    now = datetime.datetime.now()
    dateFilename = str(now.strftime("%d-%m-%Y"))
    return dateFilename

def Sheets(dateFilename):
    workbook = xlsxwriter.Workbook(dateFilename+"_Conference_List"+".xlsx")
    worksheet_1 = workbook.add_worksheet("BlockChain")
    worksheet_2 = workbook.add_worksheet("DevOps")

    colour_format = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'green'})

    noColour_format = workbook.add_format({
        'border': 1,
        'align': 'left',
        'valign': 'vcenter'})

    error_format = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'red'})

    empty_format = workbook.add_format({
        'fg_color': 'grey'})
    return worksheet_1, worksheet_2, colour_format, noColour_format, error_format, empty_format, workbook

def project_update(website_name, Block_Chain_Sheet):
    global row
    range = "A{}:E{}".format(row, row)
    Block_Chain_Sheet.merge_range(range, website_name, colour_format)
    row += 1

def empty_update(Block_Chain_Sheet):
    global row
    range = "A{}:E{}".format(row, row)
    Block_Chain_Sheet.merge_range(range, "", empty_format)
    row += 1

def heading_update(Block_Chain_Sheet):
    global row
    heading = ["Conference_Name", "Location", "Description", "Date", "Link"]
    Block_Chain_Sheet.write("A{}".format(row), heading[0], colour_format)
    Block_Chain_Sheet.write("B{}".format(row), heading[1], colour_format)
    Block_Chain_Sheet.write("C{}".format(row), heading[2], colour_format)
    Block_Chain_Sheet.write("D{}".format(row), heading[3], colour_format)
    Block_Chain_Sheet.write("E{}".format(row), heading[4], colour_format)
    row += 1

def conference_details_update(conference_details_list, Block_Chain_Sheet):
    global row
    Block_Chain_Sheet.write("A{}".format(row), conference_details_list[0], noColour_format)
    Block_Chain_Sheet.write("B{}".format(row), conference_details_list[1], noColour_format)
    Block_Chain_Sheet.write("C{}".format(row), conference_details_list[2], noColour_format)
    Block_Chain_Sheet.write("D{}".format(row), conference_details_list[3], noColour_format)
    Block_Chain_Sheet.write("E{}".format(row), conference_details_list[4], noColour_format)
    row += 1

def website_1(urls, Block_Chain_Sheet):
    global row, jsonObj
    scrapurl = UReq(urls[0])
    scraphtml = scrapurl.read()
    website_name = "blog.bizzabo.com"
    site_1 = {website_name:[]}
    jsonObj.update(site_1)
    project_update(website_name, Block_Chain_Sheet)
    heading_update(Block_Chain_Sheet)

    html_dom = soup(scraphtml, 'html.parser')
    tables = html_dom.find_all('table', {'dir': 'ltr'})
    conference_details = []
    for table in tables:
        table_rows = table.find_all("tr")
        for table_row in table_rows:
            table_data_conference_name = table_row.find_all('td')[0].text
            if "Event Name" in table_data_conference_name:
                continue
            else:
                table_data_conference_name_href = table_row.find_all('td')[0].span.a
                table_data_location = table_row.find_all('td')[1].text
                table_data_description = table_row.find_all('td')[2].text
                table_data_start_date = table_row.find_all('td')[3].text
                if table_data_conference_name_href == None:
                    continue
                conference_details.append({"Conference_Name": str(table_data_conference_name.strip()), "Location": str(table_data_location.strip()), "Description": str(table_data_description.strip()), "Date": str(table_data_start_date.strip()), "Link": str(table_data_conference_name_href["href"])})
                conference_details_list = [str(table_data_conference_name.strip()), str(table_data_location.strip()), str(table_data_description.strip()), str(table_data_start_date.strip()), str(table_data_conference_name_href["href"])]
                conference_details_update(conference_details_list, Block_Chain_Sheet)
    jsonObj[website_name].append(conference_details)
    print("Details of the website: "+ website_name+ " is updated..")
    empty_update(Block_Chain_Sheet)

def website_2(urls, Block_Chain_Sheet):
    global row, jsonObj
    scrapurl = UReq(urls[1])
    scraphtml = scrapurl.read()

    website_name = "10times.com"
    site_2 = {website_name: []}
    jsonObj.update(site_2)

    project_update(website_name, Block_Chain_Sheet)
    heading_update(Block_Chain_Sheet)

    html_dom = soup(scraphtml, 'html.parser')
    table = html_dom.find_all('table', {'class': 'table table-responsive table-hover'})

    conference_details = []

    for for_rows in table:
        table_row = for_rows.find_all("tr")
        for for_table_data in table_row:
            table_data = for_table_data.find_all("td")
            if table_data == []:
                continue
            elif table_data[0].has_attr('colspan'):
                continue
            else:
                country = str(table_data[3].a.text).strip()
                state = str(table_data[3].small.text).strip()
                address = state+", "+country
                description = str(table_data[4].text).strip()
                conference_details.append({"Conference_Name": str(table_data[1].strong.a.text).strip(),
                                           "Location": str(address.strip()),
                                           "Description": str(description.strip()),
                                           "Date": str(table_data[2].strong.text.strip()),
                                           "Link": str(table_data[1].strong.a['href']).strip()})
                conference_details_list = [str(table_data[1].strong.a.text).strip(), str(address.strip()),
                                           str(description.strip()), str(table_data[2].strong.text.strip()),
                                           str(table_data[1].strong.a['href']).strip()]
                conference_details_update(conference_details_list, Block_Chain_Sheet)
    jsonObj[website_name].append(conference_details)
    print("Details of the website: " + website_name + " is updated..")
    empty_update(Block_Chain_Sheet)

def website_3(urls, DevOps_Sheet):
    global row, jsonObj
    scrapurl = UReq(urls[2])
    scraphtml = scrapurl.read()

    website_name = "gotodevops.org"
    site_3 = {website_name: []}
    jsonObj.update(site_3)

    project_update(website_name, DevOps_Sheet)
    heading_update(DevOps_Sheet)

    html_dom = soup(scraphtml, 'html.parser')
    conference_list = html_dom.find('div', {'id': 'conferences'})
    list = conference_list.find_all("a")

    conference_details = []

    for a_list in list:
        conference_link = str(a_list['href']).strip()
        conference_details_country_title = a_list.find_all("div", {"class": "col-12"})
        conference_date = str(a_list.find("div", {"class": "col-5 col-md-6 mr-0 pr-0"}).text).strip()
        conference_title = str(conference_details_country_title[0].text).strip().split(' »')[0]
        conference_location = str(conference_details_country_title[1].text).strip()
        conference_details.append({"Conference_Name": conference_title,
                                   "Location": conference_location,
                                   "Description": "Nil",
                                   "Date": conference_date,
                                   "Link": conference_link})
        conference_details_list = [conference_title, conference_location, "Nil", conference_date,
                                   conference_link]
        conference_details_update(conference_details_list, DevOps_Sheet)
    jsonObj[website_name].append(conference_details)
    print("Details of the website: " + website_name + " is updated..")
    empty_update(DevOps_Sheet)

def website_4(urls, DevOps_Sheet):
    global row, jsonObj
    scrapurl = UReq(urls[3])
    scraphtml = scrapurl.read()

    website_name = "alltechconferences.com"
    site_4 = {website_name: []}
    jsonObj.update(site_4)

    project_update(website_name, DevOps_Sheet)
    heading_update(DevOps_Sheet)

    html_dom = soup(scraphtml, 'html.parser')
    conference_list = html_dom.find_all('section', {'id': 'loop_event_taxonomy'})

    conference_details = []

    for post_list in conference_list:
        post = post_list.find_all("article")
        for from_article_tag in post:
            conference_name = (str(from_article_tag.find("div", {"class": "event-title"}).h2.a.text.strip()))
            date_with_spaces = str(from_article_tag.find("div", {"class": "event-title"}).span.text.strip())
            date = re.sub('\s\s+', ' ', date_with_spaces)
            conference_link = str(from_article_tag.find("div", {"class": "event-title"}).h2.a["href"].strip())
            conference_location = str(from_article_tag.find("div", {"class": "entry-details"}).p.text.strip()).split(": ")[1]
            description = str(from_article_tag.find("div", {"class": "entry-summary"}).p.text.strip()).split(" Read more")[0]

            conference_details.append({"Conference_Name": conference_name,
                                       "Location": conference_location,
                                       "Description": description,
                                       "Date": date,
                                       "Link": conference_link})
            conference_details_list = [conference_name, conference_location,
                                       description, date, conference_link]
            conference_details_update(conference_details_list, DevOps_Sheet)

    jsonObj[website_name].append(conference_details)
    print("Details of the website: " + website_name + " is updated..")
    empty_update(DevOps_Sheet)

dateFilename = Date()
Block_Chain_Sheet, DevOps_Sheet, colour_format, noColour_format, error_format, empty_format, workbook = Sheets(dateFilename)

row = 1
jsonObj = {}

urls = ["https://blog.bizzabo.com/blockchain-events", "https://10times.com/top100/blockchain", "https://www.gotodevops.org/", "https://www.alltechconferences.com/etags/devops/"]

website_1(urls, Block_Chain_Sheet)
website_2(urls, Block_Chain_Sheet)

row = 1
website_3(urls, DevOps_Sheet)
website_4(urls, DevOps_Sheet)

print("Over all JSON Obj: ", jsonObj)
workbook.close()
