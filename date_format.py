import re
import datetime

def date_compare():
    now = datetime.datetime.now()
    dateFilename = str(now.strftime("%d-%m-%Y"))
    return dateFilename

def month_to_number(string):
    month_number = {
        'jan': 1,
        'feb': 2,
        'mar': 3,
        'apr': 4,
         'may':5,
         'jun':6,
         'jul':7,
         'aug':8,
         'sep':9,
         'oct':10,
         'nov':11,
         'dec':12
        }
    out_string = string.strip()[:3].lower()
    try:
        out = month_number[out_string]
        return out
    except:
        return False


def month_string_to_number(string):
    month_number = {
        'jan': "01",
        'feb': "02",
        'mar': "03",
        'apr':"04",
         'may':"05",
         'jun':"06",
         'jul':"07",
         'aug':"08",
         'sep':"09",
         'oct':"10",
         'nov':"11",
         'dec':"12"
        }
    month_string = {
        'jan': "January",
        'feb': "February",
        'mar': "March",
        'apr': "April",
        'may': "May",
        'jun': "June",
        'jul': "July",
        'aug': "August",
        'sep': "September",
        'oct': "October",
        'nov': "November",
        'dec': "December"
    }
    out_string = string.strip()[:3].lower()

    try:
        out = month_string[out_string]
        return out
    except:
        return False


def day_format(day):
    if len(day) == 1:
        return str("0" + day)
    else:
        return day


def Regex(date):
    pattern_01 = re.findall(r'^(\d{2}|\d{1})\s(\w+)(\s\-\s)(\d{2}|\d{1})\s(\w+)\s(\d{4})$', date) #dd Mm - dd mm yyyy
    pattern_02 = re.findall(r'^(\d{2}|\d{1})(\s\-\s)(\d{2}|\d{1})\s(\w+)\s(\d{4})$', date)  #dd - dd Mm yyyy
    pattern_03 = re.findall(r'^(\d{2}|\d{1})\s(\w+)\s(\d{4})$', date)  #dd MM yyyy
    pattern_04 = re.findall(r'^(\d{2}|\d{1})\s(\w+)$', date)  #dd MM
    pattern_05 = re.findall(r'^(\w+)\s(\d{2}|\d{1})(\,\s)(\d{4})$', date)  # Mm dd yyyy

    if pattern_01 != []:
        month = month_string_to_number(str(pattern_01[0][1]))
        if month == False:
            return date
        date_formated = str(str(pattern_01[0][0])+"-" + month + "-" + str(pattern_01[0][5]))
        return date_formated
    elif pattern_02 != []:
        month = month_string_to_number(str(pattern_02[0][3]))
        if month == False:
            return date
        day = day_format(str(str(pattern_02[0][0])).strip())
        date_formated = str(day + "-" + month + "-" + str(pattern_02[0][4]))
        return date_formated
    elif pattern_03 != []:
        month = month_string_to_number(str(pattern_03[0][1]))
        if month == False:
            return date
        day = day_format(str(pattern_03[0][0]).strip())
        date_formated = str(day + "-" + month + "-" + str(pattern_03[0][2]))
        return date_formated
    elif pattern_04 != []:
        month = month_string_to_number(str(pattern_04[0][1]))
        if month == False:
            return date
        day = day_format(str(pattern_04[0][0]).strip())
        date_formated = str(day + "-" + month + "-" + "2019")
        return date_formated
    elif pattern_05 != []:
        month = month_string_to_number(str(pattern_05[0][0]))
        if month == False:
            return date
        day = day_format(str(pattern_05[0][1]).strip())
        date_formated = str(day + "-" + month + "-" + str(pattern_05[0][3]))
        return date_formated
    else:
        return False

def expired_conference(compare_date):
    isIncorrect = None
    current_date_split = date_compare().split("-")
    try:
        compare_date_split = compare_date.split("-")
        current_year = int(current_date_split[2])
        compare_year = int(compare_date_split[2])
        current_month = int(current_date_split[1])
        compare_month = int(month_to_number(compare_date_split[1]))
        current_day = int(current_date_split[0])
        compare_day = int(compare_date_split[0])
        if compare_year == current_year:
            if compare_month == current_month:
                if compare_day >= current_day:
                    isIncorrect = True
                    return isIncorrect
                else:
                    isIncorrect = False
                    return isIncorrect
            elif compare_month > current_month:
                isIncorrect = True
                return isIncorrect
            elif compare_month < current_month:
                isIncorrect = False
                return isIncorrect
            else:
                isIncorrect = False
                return isIncorrect
        elif compare_year > current_year:
            isIncorrect = True
            return isIncorrect
        elif compare_year < current_year:
            isIncorrect = False
            return isIncorrect
        else:
            isIncorrect = False
            return isIncorrect
    except Exception:
         return isIncorrect
